# 文档服务

## 将Runner注册到项目中

项目结构说明:

```
项目跟路径
 ┣ src
 ┃ ┣ assets         # 各个文档资源文件，如images
 ┃ ┣ 各微服务文档文件
 ┃ ┃  ┣_sidebar.md  # 文档左侧导航
 ┃ ┣ _coverpage.md  # 文档首页导航栏 封面
 ┃ ┣ _navbar.md     # 文档右上角导航
 ┃ ┗ index.html     # 入口文件
 ┣ .gitlab-ci.yml   # Gitlab持续集成脚本
 ┣ docker-compose.yml   # Gitlab-Runner启动文档容器脚本
 ┣ Dockerfile       # 构建脚本
 ┗ Dockerfile.onbuild
```



