![logo](assets/_media/icon.svg)
 
<h1>应用支撑项目文档</h1>

<p>
	<a href="#/RGP-MESSAGE/01.introduction/background.md">消息队列</a>
    <a href="#">应用交付</a>
    <a href="#">运行环境</a>
    <a href="#">资源管理</a>
    <br/>
    <a href="#">用户中心</a>
    <a href="#">权限中心</a>
    <a href="#">服务框架</a>
    <a href="#">后端</a>
    <br/>
    <a href="#">前端</a>
    <a href="#">日志采集</a> 
    </br>
    <a style="display:none;">none</a>
</p>

[comment]: <> (<!-- _coverpage.md -->)

[comment]: <> (<img src="assets/_media/favicon.ico" width="160px" style="border-radius: 50%">)

[comment]: <> (# docsify <small>3.5</small>)

[comment]: <> (> 一个神奇的文档网站生成器。)

[comment]: <> (- 简单、轻便 &#40;压缩后 ~21kB&#41;)

[comment]: <> (- 无需生成 html 文件)

[comment]: <> (- 众多主题)

[comment]: <> ([GitHub]&#40;https://github.com/docsifyjs/docsify/&#41;)

[comment]: <> ([Get Started]&#40;#docsify&#41;)